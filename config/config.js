exports.noTeamOptions = {
     boardNumber: -1
}

exports.wolfRedOptions = {
    sprintNameIncludes: 'Red',
    //Main issueTypes
    arrStrPBIIssueTypes: ["Development Sub-Task"],
    arrStrAllPBIIssueTypes: ["Development Sub-Task"],
    //All statuses that indicate issue cycle time should be included
    strClosedStatus: ["Awaiting Release","Closed","Ready for Check-In"],
    strITStory: '',
    strDevInProgress: "In Progress",
    strClosed: 'Closed',
    boardNumber: 380,
    sprintIndexStartAt: 0,
    jiraHost: 'jira.hds.hscic.gov.uk'
}

exports.wolfBlueOptions = {
    sprintNameIncludes: 'Blue',
    //Main issueTypes
    arrStrPBIIssueTypes: ["Development Sub-Task"],
    arrStrAllPBIIssueTypes: ["Development Sub-Task"],
    //All statuses that indicate issue cycle time should be included
    strClosedStatus: ["Awaiting Release","Closed","Ready for Check-In"],
    strITStory: 'Development Sub-Task',
    strDevInProgress: "In Progress",
    strClosed: 'Closed',
    boardNumber: 380,
    sprintIndexStartAt: 0,
    jiraHost: 'jira.hds.hscic.gov.uk'
}

exports.SidOptions = {
    sprintNameIncludes: 'DoS Sprint',
    //Main issueTypes
    //arrStrPBIIssueTypes: ["Story", "Bug", "Test", "Task (Detached) - Exeter"],
    arrStrPBIIssueTypes: ["Story"],
    arrStrAllPBIIssueTypes: ["Story", "Bug", "Test", "Task (Detached) - Exeter"],
    //All statuses that indicate issue cycle time should be included
    //strClosedStatus: ["Done"],
    strDevInProgress: "In Development",
    strClosed: 'Closed',
    dateProcessChangedDate: new Date("09-May-2018"),
    strNewClosed:'Done',
    boardNumber: 667,
    sprintIndexStartAt: 20,
    jiraHost: 'jira.digital.nhs.uk'
}

exports.AxeOptions = {
    sprintNameIncludes: '*',
    //Main issueTypes
    arrStrPBIIssueTypes: ["Story", "Bug", "Test", "Task (Detached) - Exeter"],//All statuses that indicate issue cycle time should be included
    arrStrAllPBIIssueTypes: ["Story", "Bug", "Test", "Task (Detached) - Exeter"],//All statuses that indicate issue cycle time should be included
    //All statuses that indicate issue cycle time should be included
    //strClosedStatus: ["Done"],
    strITStory: 'Story',
    strDevInProgress: "In Progress",
    strClosed: 'Done',
    boardNumber: 651,
    sprintIndexStartAt: 60,
    jiraHost: 'jira.digital.nhs.uk'
}

exports.BCSSOptions = {
    sprintNameIncludes: '*',
    //All statuses that indicate issue cycle time should be included
    arrStrPBIIssueTypes: ["Story", "Bug", "Test", "Task (Detached) - Exeter"],//All statuses that indicate issue cycle time should be included
    arrStrAllPBIIssueTypes: ["Story", "Bug", "Test", "Task (Detached) - Exeter"],//All statuses that indicate issue cycle time should be included
    strClosedStatus: ["Done"],
    strITStory: 'Story',
    strDevInProgress: "In Development",
    strClosed: 'Closed',
    boardNumber: 779,
    sprintIndexStartAt: 0,
    jiraHost: 'jira.digital.nhs.uk',
    dataDirName: ''
}
exports.BCSSEXEFITAPIOptions = {
    sprintNameIncludes: 'EXEAPI',
    originBoardId: 775,
    dataDirName: 'BCSS'
}
exports.BCSSPurpleOptions = {
    sprintNameIncludes: 'Purple',
    originBoardId: 1030,
    dataDirName: 'BCSS'
}
exports.BCSSFITLoggingOptions = {
    sprintNameIncludes: 'FITLogging',
    originBoardId: 784,
    dataDirName: 'BCSS'
}
exports.BCSSFITPilotOptions = {
    sprintNameIncludes: 'FITPilot',
    originBoardId: 778,
    dataDirName: 'BCSS'
}

exports.BCSSBTAOptions = {
    sprintNameIncludes: '*',
    //All statuses that indicate issue cycle time should be included
    arrStrPBIIssueTypes: ["Story", "Bug", "Test", "Task (Detached) - Exeter"],//All statuses that indicate issue cycle time should be included
    arrStrAllPBIIssueTypes: ["Story", "Bug", "Test", "Task (Detached) - Exeter"],//All statuses that indicate issue cycle time should be included
    strClosedStatus: ["Done"],
    strITStory: 'Story',
    strDevInProgress: "In Development",
    strClosed: 'Closed',
    boardNumber: 653,
    sprintIndexStartAt: 0,
    jiraHost: 'jira.digital.nhs.uk'
}