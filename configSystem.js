const config = require('./config/config.js');

var currentTeam = 'wolfBlue';
var currentTeamOptions = config.wolfBlueOptions;

exports.setCurrentTeam = function(newCurrentTeam) {
    currentTeam = newCurrentTeam;
    console.log("changing team");
    switch(currentTeam) {
        case 'wolfRed':
            currentTeamOptions =  config.wolfRedOptions;
            break;
        case 'wolfBlue':
            currentTeamOptions =  config.wolfBlueOptions;
            break;
        case 'Sid':
            currentTeamOptions =  config.SidOptions;
            break; 
        case 'Axe':
            currentTeamOptions =  config.AxeOptions;
            break;        
        case 'BCSS':
            currentTeamOptions =  config.BCSSOptions;
            break;
        case 'BCSSEXEAPI':
            currentTeamOptions = config.BCSSOptions;
            currentTeamOptions.sprintNameIncludes = config.BCSSEXEFITAPIOptions.sprintNameIncludes;
            currentTeamOptions.originBoardId = config.BCSSEXEFITAPIOptions.originBoardId;
            currentTeamOptions.dataDirName = config.BCSSEXEFITAPIOptions.dataDirName;
            break;
        case 'BCSSPurple':
            currentTeamOptions =  config.BCSSOptions;
            currentTeamOptions.sprintNameIncludes = config.BCSSPurpleOptions.sprintNameIncludes;
            currentTeamOptions.originBoardId = config.BCSSPurpleOptions.originBoardId;
            currentTeamOptions.dataDirName = config.BCSSPurpleOptions.dataDirName;
            break;
        case 'BCSSFITLogging':
            currentTeamOptions = config.BCSSOptions;
            currentTeamOptions.sprintNameIncludes = config.BCSSFITLoggingOptions.sprintNameIncludes;
            currentTeamOptions.originBoardId = config.BCSSFITLoggingOptions.originBoardId;
            currentTeamOptions.dataDirName = config.BCSSFITLoggingOptions.dataDirName;
            break;
        case 'BCSSFITPilot':
            currentTeamOptions =  config.BCSSOptions;
            currentTeamOptions.sprintNameIncludes =  config.BCSSFITPilotOptions.sprintNameIncludes;
            currentTeamOptions.originBoardId =  config.BCSSFITPilotOptions.originBoardId;
            currentTeamOptions.dataDirName = config.BCSSFITPilotOptions.dataDirName;
            break;
        case 'BCSSBTA':
            currentTeamOptions =  config.BCSSBTAOptions;
            break;
            
        default:
            currentTeam = 'No Team';
            currentTeamOptions =  config.noTeamOptions;
    }
    //console.log(currentTeam);
}

exports.getCurrentTeam = function() {
    return currentTeam;
}

exports.getSprintNameIncludes = function() {
    return currentTeamOptions.sprintNameIncludes;
};
//exports.getStrClosedStatus = function() {
//    return currentTeamOptions.strClosedStatus;
//};
exports.getStrITStory = function() {
    return currentTeamOptions.strITStory;
};
exports.getStrDevInProgress = function() {
    return currentTeamOptions.strDevInProgress;
};
exports.getStrClosed = function(transactionDate) {
    var strClosed = currentTeamOptions.strClosed;
    if (currentTeam == 'Sid' && (transactionDate > currentTeamOptions.dateProcessChangedDate)) {
        strClosed = currentTeamOptions.strNewClosed;
    }
    return strClosed;
};
exports.getBoardNumber = function() {
    return currentTeamOptions.boardNumber;
};
exports.getJiraHost = function() {
    return currentTeamOptions.jiraHost;
};

exports.getPBIIssueTypes = function() {
    var arrIssueTypes =[];
    if (currentTeamOptions.hasOwnProperty('arrStrPBIIssueTypes')) {
        arrIssueTypes = currentTeamOptions.arrStrPBIIssueTypes;
    }
    return arrIssueTypes;
};
exports.getExtendedPBIIssueTypes = function() {
    var arrIssueTypes =[];
    if (currentTeamOptions.hasOwnProperty('arrStrAllPBIIssueTypes')) {
        arrIssueTypes = currentTeamOptions.arrStrAllPBIIssueTypes;
    }
    return arrIssueTypes;
};

exports.getJiraURL = function() {
    return 'https://' + currentTeamOptions.jiraHost + '/browse/';
};

exports.mapFixVersion = function(fixVersion) {
    var returnFixVersion = fixVersion;
    if (currentTeam == "Sid") {
        switch (fixVersion) {
            case "4.5.0":
                returnFixVersion = "4.6.0";
                break;
            case "4.8.0":
                returnFixVersion = "4.9.0";
                break;
            case "4.15.0":
                returnFixVersion = "4.16.0";
                 break;
            default:
                returnFixVersion = fixVersion;
        }
    }

    return returnFixVersion;
}

exports.getSprintIndexStartAt  = function() {
    return currentTeamOptions.sprintIndexStartAt;
}

exports.getPBIIssueType = function(issuetype, summary) {
    var strIssueType = issuetype;
    switch(currentTeam) {
        case "Sid":
            switch (issuetype) {
                case "Task (Detached) - Exeter":
                    strIssueType = "Tech Task";
                    // deliberate drop through, to rename spike
                case "Story":
                    if(summary.substring(0,5).toLowerCase() == "spike") {
                        strIssueType = "Spike";
                    }
                    break;

                default:
            }
            break;
        case "BCSS":
            switch (issuetype) {
                case "Task (Detached) - Exeter":
                    strIssueType = "Tech Task";
                    break;   
                default:    
                    //Do nothing
            }      
        default:
    }
    return strIssueType;
}

exports.getDataDir  = function() {
    return (currentTeamOptions.hasOwnProperty('dataDirName')) ? currentTeamOptions.dataDirName : currentTeam;

}
exports.getDataFilePrefix = function() {
    return (currentTeamOptions.hasOwnProperty('dataDirName')) ? currentTeamOptions.dataDirName : currentTeam;
}

exports.IsValidSprint = function(sprintName, originBoardId) {
    var isValidSprint = false;
    if (originBoardId == "undefined" || currentTeamOptions.hasOwnProperty('originBoardId') == false) {
        var sprintNameInclude = currentTeamOptions.sprintNameIncludes;
    
        isValidSprint =  sprintNameInclude == "*" || sprintName.includes(sprintNameInclude);
    } else {
        isValidSprint = (originBoardId == currentTeamOptions.originBoardId);
    }
    //console.log(sprintName, originBoardId, isValidSprint)
    return isValidSprint;
}

exports.IsFinishedInCurrentSprint = function(status, sprintName , lastSprintName) {
    var isFinishedInCurrentSprint;
    if (status == "Done") {
        switch(currentTeam) {
            case "Axe":
                isFinishedInCurrentSprint = (sprintName > lastSprintName);
                break;
            default:
                isFinishedInCurrentSprint = (sprintName == lastSprintName)
        }
    } else {
        isFinishedInCurrentSprint = false;
    }
    return isFinishedInCurrentSprint;
}

 exports.getParent = function(objJiraParent, axeParent) {
    var parent = "";
    if (currentTeam == "Axe") {
        parent = axeParent
    } else {
        if (typeof objJiraParent !== 'undefined') {
            parent = objJiraParent.key;
        }            
	}
	return parent;
}