var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000,
  //mongoose = require('mongoose'),
  //Task = require('./api/models/jiraAPIModel'), //created model loading here
  bodyParser = require('body-parser');

var jiraFns = require('./api/controllers/jiraHelper');

  // Turn on ZIP compression
var compression = require('compression');
app.use(compression());



// mongoose instance connection url connection
//mongoose.Promise = global.Promise;
//mongoose.connect('mongodb://localhost/Tododb'); 


app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());


var routes = require('./api/routes/jiraAPIRoutes'); //importing route
routes(app); //register the route

//jiraFns.initJiraClient('matthew.dodwell','RedGreenJira1!', 'BCSFITApi');
jiraFns.initJiraClient('markgreen','Password1!', 'Sid');

//jiraFns.initJiraClient('matthew.dodwell','Sameagle1!', 'Sid');

//jiraFns.initJiraClient('joel.layton','', 'wolfRed');

app.listen(port);


console.log('todo list RESTful API server started on: ' + port);
