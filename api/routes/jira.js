const jiraFns = require('../controllers/jiraHelper');
const config = require('../../configSystem.js');
const fs = require('fs');
const moment = require('moment-business-days');

exports.initialise = function(req, res) {
    var username = req.params.username;
    var password = req.params.password;
    var teamname = req.params.teamname;
    
    console.log(username +", "+ teamname);
    
    jiraFns.initJiraClient(username, password, teamname);
    
    console.log("Initialised with team:" + teamname);
     
    var results={};
    res.status(200);
    res.json(results);
};

// 1
exports.getSprintsByTeam = function(req, res) {
    var isInitalised = jiraFns.isJiraInitialised("dummy");
    if (!isInitalised) {
        throw new Error("Not logged on");
    }

    config.setCurrentTeam(req.params.teamLabel);
    if (config.getCurrentTeam() == 'No Team') {
        throw new Error("Team Name not recognised");
    }
    jiraFns.getAllSprintDataForBoard(config.getBoardNumber())
    .then(function(results) {
        res.json(results);
    }, errHandler);
}

//2
exports.getStoriesByTeam = function(req, res) {
    config.setCurrentTeam(req.params.teamLabel);
    jiraFns.getCycleDataForBoard(config.getBoardNumber())
    .then(function(results) {
        res.json(results);
    }, errHandler);
}

//3
exports.getStoriesByTeamBySprint = function(req, res) {
    config.setCurrentTeam(req.params.teamLabel);
    var sprintId = req.params.sprintId;
    jiraFns.getSprintDetails(config.getBoardNumber(), sprintId)
    .then(function (sprintDetails){
        var bCompletedStoryOnly = false;
        var results = jiraFns.getFullSprintData(sprintDetails, "extendedPBIs", bCompletedStoryOnly);
        return results;
    })
    .then(function (sprint){
         return sprint;
    })
    .then(function(sprint) {
        res.json(sprint);
    })
    .catch(function(error) {
        console.log(error);
    })
}


exports.getFullSprintDetailsByTeam = function(req, res) {
    config.setCurrentTeam(req.params.teamLabel);
    var bCompletedStories = req.query.complete =="true" ? true : false;
    //console.log(bCompletedStories);
    jiraFns.getCycleDataForBoardGroupedBySprint(config.getBoardNumber(), bCompletedStories)
    .then(function(results) {
        res.json(results);
    }, errHandler);
}

exports.testDays = function (req, res) {
    
    var results = jiraFns.testDays();

    res.json(results);
}
/*
exports.getScoringPBIsByTeamBySprint = function(req, res) {
    var teamLabel = req.params.teamLabel;
    config.setCurrentTeam(teamLabel);

    var sprintId = req.params.sprintId;

    jiraFns.getScoringPBIDataForSingleSprint(config.getBoardNumber(), sprintId)
    .then(function(results) {
        res.json(results);
    }, errHandler);
}



exports.getTimedStoriesByTeamBySprint = function(req, res) {
    var teamLabel = req.params.teamLabel;
    config.setCurrentTeam(teamLabel);

    var sprintId = req.params.sprintId;

    jiraFns.getPBIDataForSprint(config.getBoardNumber(), sprintId, "extendedPBIs")
    .then(function(results) {

        var startDate = moment(results[0].sprintSummary.startDate);

        var sprintLength = 10;
        for (var ii=1;ii<sprintLength;ii++) {
            var nextDay = startDate.businessAdd(ii)
           console.log(ii+": "+nextDay.format('ddd DD-MMM-YYYY hh:mm:ss')); 
        }
        res.json(results);
    })
    .catch(function(error) {
        console.log(error);
    });
}



exports.getAllPBIsByTeamBySprint = function(req, res) {
    var teamLabel = req.params.teamLabel;
    config.setCurrentTeam(teamLabel);

    var sprintId = req.params.sprintId;
    
    jiraFns.getAllPBIDataForSingleSprint(config.getBoardNumber(), sprintId)
    .then(function(results) {
        res.json(results);
    }, errHandler);
}



 exports.getSprintIdsByBoard = function(req, res) {
    console.log("getSprintIdsByBoard");
    jiraFns.getAllSprintDataForBoard(req.params.boardId)
    .then(function(results) {
        var arrId = results.map(results => results.id);
        res.send(arrId);
    }, errHandler);
}
exports.getAllSprintDataByBoardId = function(req, res) {
		
    jiraFns.getAllSprintDataForBoard(req.params.boardId)
    .then(function(results) {
        res.json(results);
    }, errHandler);
}

exports.getStoriesByBoardId = function(req, res) {
    jiraFns.getCycleDataForBoard(req.params.boardId)
    .then(function(results) {
        //console.log("Results: "+results.length);
        res.json(results);
    }, errHandler);
}

exports.getNextSprintByBoardId = function(req, res) {
    jiraFns.getNextSprintData(req.params.boardId)
    .then(function(results) {
        console.log("Results: "+results.length);
        res.json(results);
    }, errHandler);
}


//
exports.getGroupedStoriesByTeam = function(req, res) {
    config.setCurrentTeam(req.params.teamLabel);
    jiraFns.getCycleDataForBoardGroupedBySprint(config.getBoardNumber())
    .then(function(results) {
        res.json(results);
    }, errHandler);
}
*/



	/*
	app.get('/allStoryDataCSVByBoardId/:boardId', function(req, res) {
		jiraFns.getAllItemsFromBoard(req.params.boardId)
		.then(function(results) {
			
			res.send(convertJsonResultsToCSV(results));
		}, errHandler);
	});
	**/
	/*
	app.get('/allStoryDataforSprint/:sprintId', function(req, res) {
		jiraFns.getAllSprintData(req.params.sprintId)
		.then(function(results) {
			console.log("Results: "+results.length);
			res.json(results);
		}, errHandler);
	});	
    */
//////////////////////////////////////////////////////////////////
// test functions
//////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////
// private functions
//////////////////////////////////////////////////////////////////

function convertJsonResultsToCSV(results) {
	var resultsCSV = "";
	// process JSON to CSV
	for (const jsonItem of results) {
	/*	
		"id": "140967",
		"key": "NATSMOS-42",
		"type": "Story",
		"status": "Closed",
		"resolution": "Done",
		"cycleTime": 26.1
		*/
		if (jsonItem.cycleTime != 0) {
			resultsCSV += jsonItem.id+","+jsonItem.key+","+jsonItem.type+","+jsonItem.status+","+jsonItem.resolution+","+jsonItem.cycleTime+"<br>";
		}
	}
	console.log("resultsCSV: "+resultsCSV.length);
	return resultsCSV;
}

var errHandler = function(err) {
    console.log(err.statusCode);
    console.log(err.message);
 }
