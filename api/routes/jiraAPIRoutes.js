'use strict';
module.exports = function(app) {
  var mcache = require('memory-cache');
  var jiraRoutes = require('./jira.js')



	var cache = (duration) => {
	  return (req, res, next) => {
		let key = '__express__' + req.originalUrl || req.url
		let cachedBody = mcache.get(key)
		if (cachedBody) {
		  res.send(cachedBody)
		  return
		} else {
		  res.sendResponse = res.send
		  res.send = (body) => {
			mcache.put(key, body, duration * 1000);
			res.sendResponse(body)
		  }
		  next()
		}
	  }
	}

	app.use(function(req, res, next) {
	  res.header("Access-Control-Allow-Origin", "*");
	  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	  next();
	});

	////////////////////////////////////////////////////
	// Team data routes
	////////////////////////////////////////////////////
	app.get('/initialise/:username/:password/:teamname', jiraRoutes.initialise);

	//app.get('/team/:teamLabel/sprints', 	cache(1000),jiraRoutes.getSprintsByTeam);
	app.get('/team/:teamLabel/sprints', 	jiraRoutes.getSprintsByTeam);

	app.get('/team/:teamLabel/sprints/fullDetails', 	jiraRoutes.getFullSprintDetailsByTeam);
	
//	app.get('/team/:teamLabel/stories', 	cache(1000), jiraRoutes.getStoriessByTeam);
app.get('/team/:teamLabel/stories', 							jiraRoutes.getStoriesByTeam);
app.get('/team/:teamLabel/storiesGroupedBySprint',jiraRoutes.getStoriesByTeamBySprint);
app.get('/team/:teamLabel/:sprintId/stories', 		jiraRoutes.getStoriesByTeamBySprint);	

	
	// app.get('/team/:teamLabel/:sprintId/pbis', 			jiraRoutes.getScoringPBIsByTeamBySprint);
	// app.get('/team/:teamLabel/:sprintId/allPbis', 	jiraRoutes.getAllPBIsByTeamBySprint);

	//app.get('/team/:teamLabel/:sprintId/timedStories', 			jiraRoutes.getTimedStoriesByTeamBySprint);
  app.get('/testDays', 			jiraRoutes.testDays);
	
	
	////////////////////////////////////////////////////
	// Board Id  routes
	////////////////////////////////////////////////////
	// app.get('/board/:boardId/sprint', 		jiraRoutes.getAllSprintDataByBoardId);
	// app.get('/board/:boardId/sprintIds', 	jiraRoutes.getSprintIdsByBoard);
	// app.get('/board/:boardId/stories', 		jiraRoutes.getStoriesByBoardId);
	// app.get('/board/:boardId/nextSprint', jiraRoutes.getNextSprintByBoardId);	
	// app.get('/board/:boardId/rawData',  	jiraRoutes.getRawDataByBoardId);

	// app.get('/board/:boardId/rawData',  	jiraRoutes.getRawDataByBoardId);

	////////////////////////////////////////////////////
	// Sprint Id routes
	////////////////////////////////////////////////////




	////////////////////////////////////////////////////
	// Drop through routes
	////////////////////////////////////////////////////
	app.use(function (req, res, next) {
		res.status(404).send("Sorry can't find that!")
	})
	app.use(function (error, req, res, next) {
		res.json({message:error.message});
	})
}



