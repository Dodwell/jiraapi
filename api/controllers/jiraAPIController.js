'use strict';


var jiraFns = require('./jiraHelper');


exports.listAllSprints = function(req, res) {
  console.log("listAllSprints");
  var boardId = '783'; 
  return jiraFns.getAllSprintIdsForBoard(boardId);
};

exports.getAllItemsFromBoard = function(boardId) {
  console.log("getAllItemsFromBoard"); 
  return jiraFns.getAllItemsFromBoard(boardId);
};





