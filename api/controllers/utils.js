/////////////////////////////////////////////////////////////////////////
// Utility Functions
/////////////////////////////////////////////////////////////////////////
exports.dateDiffInDays = function(date1, date2) {
	var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
	var diff = Math.round(Math.abs((date1 - date2) / (1000.0 * 60 * 60 *24) )*10) / 10.0;
	return (isNaN(diff) ? 0 : diff);
}

exports.formattedDate = function(d = new Date) {
  let month = String(d.getMonth() + 1);
  let day = String(d.getDate());
  const year = String(d.getFullYear());

  if (month.length < 2) month = '0' + month;
  if (day.length < 2) day = '0' + day;

  return `${day}/${month}/${year}`;
}

exports.formattedShortDate =function(d = new Date) {
  let month = String(d.getMonth() + 1);
  let day = String(d.getDate());
 
  if (month.length < 2) month = '0' + month;
  if (day.length < 2) day = '0' + day;

  return `${day}/${month}`;
}

exports.createEmptyFile = function(fileName) {
	fs.closeSync(fs.openSync(fileName, 'w'));
}

function writeFileLine(line, fileName, bComma=false) {
	var printLine = line;
	if (bComma) {
		printLine+=",";
	}
	printLine+="\n";
	fs.appendFileSync(fileName, printLine, (err) => {
		if (err) throw err;
		//console.log('The file has been saved!');
	});
}
 
exports.outputToJsonFile = function(arrJson, fileName) {
	console.log("Writing file: Len- "+arrJson.length);
	var fullFileName = fileName;
	//for (const jsonItem of arrJson) {
		try {
			createEmptyFile(fullFileName);
			//writeFileLine(JSON.stringify(arrJson), fileName, false);
			fs.writeFileSync(fullFileName, JSON.stringify(arrJson), function(err) {
				if (err) throwerr;
			
			})
		} catch (error) {
			console.log("Failed write");
		}
	//}
	console.log(arrJson.length);
}



var errHandler = function(err) {
    console.log(err.statusCode);
   console.log(err.message);
}





