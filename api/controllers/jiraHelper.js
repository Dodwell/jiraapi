var JiraApi = require('jira-client');
const fs = require('fs');
const config = require('../../configSystem.js')
//const moment = require('moment')

const moment = require('moment-business-days');

configBusinessDays();




//require('https').globalAgent.options.ca = require('ssl-root-cas/latest').create();
var jira;  // Module variable to hold the 

exports.initJiraClient =function(username, password, teamname) {

	config.setCurrentTeam(teamname);
	var jiraHostURL = config.getJiraHost();
	// Initialize
	jira = new JiraApi({
	  protocol: 'https',
	  host: jiraHostURL,
	  username: username,
	  password: password,
	  apiVersion: '2',
	  strictSSL: false 
	}); 

	console.log("Jira Initialised: "+jiraHostURL);
}

exports.isJiraInitialised = function(dummy) {
	console.log(typeof jira);
	var isNotInitialised = (typeof jira == 'undefined');
	
	return !isNotInitialised;
}


exports.getAllSprintDataForBoard = async function(boardId, ) {
  console.log("getAllSprintDataForBoard: "+boardId);
  
  var earliestStartDate = new Date(2018, 07, 01);
  var strSprintActive = "active";
	
  var sprintNameInclude = config.getSprintNameIncludes();
  var strSprintClosed = "closed";  
	
  try {
	  
	var arrResults = await getSprintDataFromAPIOrCachedFile(boardId);
	//console.log("Got Data: "+arrResults.length);

	// Check if sprints file is within date
	for (const sprint of arrResults) {
		if ((sprint.state == strSprintActive) && config.IsValidSprint(sprint.name, sprint.originBoardId) ) {
			var endDate = new Date(sprint.endDate);
			var msg = "Sprints up to date.";
			if (endDate < new Date()) {
				msg = "ERROR: Sprint out of date!! -";
			}
			console.log(msg +sprint.id+" "+sprint.state+" End Date: "+endDate);
		}
	}
	
	// Filter sprint list on Start Date, closed state and name
	var arrSprints = [];
	for (const sprint of arrResults) {
		var startDate 	= new Date(sprint.startDate);
		
		//console.log(sprint.name, (startDate >= earliestStartDate), (sprint.state == strSprintClosed || sprint.state == strSprintActive), config.IsValidSprint(sprint.name, sprint.originBoardId));
		if ((startDate >= earliestStartDate) 
		 && (sprint.state == strSprintClosed || sprint.state == strSprintActive) 
	     &&  config.IsValidSprint(sprint.name, sprint.originBoardId) ) {
			arrSprints.push(sprint);
			//console.log(sprint.name+" "+sprint.id+" "+sprint.state+" "+formattedDate(startDate)+" "+sprintNameInclude);
		}
	};
 
  } catch (err) {
	console.log(err.statusCode);
    console.error(err.message);
	console.log("error")
  }
 
  return arrSprints;
}


exports.getCycleDataForBoard = async function(boardId) {
	const results = await exports.getAllSprintDataForBoard(boardId)
	.then(function (arrSprints) {
		var bCompletedStoryOnly = true;
		var arrPromisesFullDetails = [];

		for (const sprint of arrSprints) {
			var fullSprintDetails = exports.getFullSprintData(sprint, "extendedPBIs", bCompletedStoryOnly);
			arrPromisesFullDetails.push(fullSprintDetails);
		}
		return Promise.all(arrPromisesFullDetails);
	})
	.then(function (arrSprintFullDetails){
		var pbis=[];
		for (const sprint of arrSprintFullDetails) {
			// append the pbis from the sprint on to the return array
			pbis.push(...sprint.pbis);
		}
		return pbis;
	})
	.catch(function(error){
		console.log(error)
	});
	
	return results;
}
exports.getCycleDataForBoardGroupedBySprint = async function(boardId, bCompletedStoryOnly) {
	const results = await exports.getAllSprintDataForBoard(boardId)
	.then(function (arrSprints) {
		var arrPromisesFullDetails = [];

		for (const sprint of arrSprints) {
			var fullSprintDetails = exports.getFullSprintData(sprint, "extendedPBIs", bCompletedStoryOnly);
			arrPromisesFullDetails.push(fullSprintDetails);
		}
		return Promise.all(arrPromisesFullDetails);
	})
	.catch(function(error){
		console.log(error)
	});
	
	return results;
}


/* Refactr=ored code to get one sprints worth
 */

exports.getSprintDetails = async function(boardId, sprintId) {

	try {
		var sprintDetails = await getJiraSprintDetails(boardId, sprintId);
		
	} catch (error) {
		console.log(error);
	}
	return sprintDetails;
}

exports.getFullSprintData = async function(sprintDetails, strIssueTypes, bCompletedStoryOnly) {
	var strClosed = "closed";
	try {
		if (sprintDetails.id == 2432) {
			console.log("test")
		}	
		
		const results = await getJiraIssueDataFromAPIOrCachedFile(sprintDetails.id, (sprintDetails.state==strClosed));

		var outputList =filterListByPBIType(results, strIssueTypes);
		
		// Add sprint summary details to each pbi
		//addSprintSummaryToPBIs(sprintDetails, outputList)
	
		
		var processedPBIs = exports.processSprintIssuesAndAddCycleTime(outputList, 
																	sprintDetails.name, 
																	new Date(sprintDetails.startDate),
																	new Date(sprintDetails.endDate), 
																	bCompletedStoryOnly);

		for (const pbi of processedPBIs) {
			if (pbi.realStatus=="Done") {
				var subTaskTimes =  getStartAndEndDateForListOfSubtasksInSprint(results, pbi.subTasks);
				pbi.calculatedCycleTime = subTaskTimes.cycleTime;
				pbi.subTaskTimes = subTaskTimes;
			} else {
				pbi.calculatedCycleTime = -1;
			}
		}


		var totals = getTotals(processedPBIs)

		var sprint = { sprintId: sprintDetails.id,
					name: sprintDetails.name,
					startDate: sprintDetails.startDate,
					endDate: sprintDetails.endDate,
					totals: totals,
					pbis: processedPBIs}

		return sprint;
	} catch (error) {
		console.log(error);
	}
}

function filterListByPBIType(results, strIssueTypes) {
	var outputList;
	var arrAllowedIssueTypes;
	switch (strIssueTypes) {
		case "stories":
			arrAllowedIssueTypes = config.getPBIIssueTypes();
			outputList = results.filter(pbi => arrAllowedIssueTypes.includes(pbi.fields.issuetype.name));
			break;
		case "extendedPBIs":
			arrAllowedIssueTypes = config.getExtendedPBIIssueTypes();
			outputList = results.filter(pbi => arrAllowedIssueTypes.includes(pbi.fields.issuetype.name));
			break;
		case "all":			
		default:
			outputList = results;
	}
	return outputList;

}

function getTotals(pbis) {
	var totals = {
		numStories: 0,
		numStoriesWithFixVersion: 0,
		numSpikes: 0,
		numBugs: 0,
		numUnknown: 0,
		numTaskDetached: 0,
		totalUserStoryPoints: 0,
		numStoriesWithoutPoints: 0,
		numStoriesCompleted:0,
		numStoryPointsCompleted:0,
		aveCycleTimeForStories:0,
		stdDevCycletime:0,
		percentStoryComplete: 0,
		percentPointsComplete: 0,
		numStoriesAdded: 0,
		numStoriesWithoutCT: 0
	}

	var arrCompletedStoryPts =[];
	
	for (const pbi of pbis) {
		switch (pbi.type) {
			case "Spike":
				totals.numSpikes++;
				break;
			case "Story":	
			case "Development Sub-Task":				
				totals.numStories++;
				if (pbi.realStatus=="Done") {
					totals.numStoriesCompleted++;
					var cycleTime = Math.max(pbi.cycleTime, pbi.calculatedCycleTime)
					if (cycleTime>0) {
						arrCompletedStoryPts.push(cycleTime);	
					} else {
						totals.numStoriesWithoutCT++;
					}
				}

				if (pbi.fixVersion.length > 0) {
					totals.numStoriesWithFixVersion++;
				} 
				break;
			case "Tech Task":
				totals.numTaskDetached++;
				break;
			case "Bug":
				totals.numBugs++;
				break;
			default:
				console.log("Unknown PBI type: "+pbi.type);
				totals.numUnknown++
		}
		if (pbi.storyPoints > 0) {
			totals.totalUserStoryPoints += pbi.storyPoints;
			if (pbi.realStatus == "Done") {
				totals.numStoryPointsCompleted += pbi.storyPoints;
			}
		} else {
			if (pbi.type == "Story" || pbi.type == "Development Sub-Task") {
				totals.numStoriesWithoutPoints++;
			}
		}
		if (pbi.wasAdded) {
			totals.numStoriesAdded++;
		}

	}

	//round to 1 or 2 dp
	totals.aveCycleTimeForStories 	= Math.round(average(arrCompletedStoryPts)*10)/10;
	totals.stdDevCycletime 			= Math.round(standardDeviation(arrCompletedStoryPts)*10)/10;
	totals.percentStoryComplete		= Math.floor(totals.numStoriesCompleted / totals.numStories *100);
	totals.percentPointsComplete	= Math.floor(totals.numStoryPointsCompleted / totals.totalUserStoryPoints *100);

	return totals;
}


async function getJiraSprintDetails(boardId, sprintId) {
	const sprints = await exports.getAllSprintDataForBoard(boardId);
	var sprint = sprints.find(d => d.id == sprintId);
	return sprint;
}


exports.viewTransitions = function(jiraIssue) {
	var arrResults = [];
	for (const historyItem of jiraIssue.changelog.histories) {
		for (const item of historyItem.items) {
			var data = {id2: 			historyItem.id,
						"fromString": 	item.fromString,
						toString: 		item.toString 
						};
			arrResults.push(data);
		}
	};
	return arrResults
}




 
exports.processSprintIssuesAndAddCycleTime = function(issues, sprintName, sprintStartedDate, sprintClosedDate, bCompletedStoryOnly ) {
	//console.log("processSprintIssuesforCycleTime");

	var arrJson = [];
	var maxDaysToShow = 120;
	//const arrClosedStatuses = config.getStrClosedStatus();
	//const strITStory = config.getStrITStory();
	//const arrAllowedReportingIssuetypes = config.getPBIIssueTypes();
	
	issues.forEach(function(issueInSprint) {
	
		var bHasChangeLog = issueInSprint.hasOwnProperty('changelog') && issueInSprint.changelog.hasOwnProperty('histories')
		// Get data from changelog (finished sprint, )
		if (bHasChangeLog) {
			// if (issueInSprint.key=="PD-2077"){
			// 	console.log(issueInSprint.key);
			// }
			var cycleTimeData = analyseChangelogHistories(issueInSprint.changelog.histories);
		}

		// There could be multiple records for a single PBI if it's spanned multiple sprints 
		//   because the data is fetched in sprint series at present (may not always be the case if api is implemented)
		if (issueInSprint.key == "BSS-7749") {
			console.log("Test ");
		}
		var lastSprintName = (cycleTimeData.hasOwnProperty('lastSprint')) ?  cycleTimeData.lastSprint : sprintName;
		//var bIsAllowedPbi = arrAllowedReportingIssuetypes.includes(issueInSprint.fields.issuetype.name);
		var bCompletedDateInThisSprint = ( sprintStartedDate < cycleTimeData.closedDate) && (cycleTimeData.closedDate < sprintClosedDate);
		var bFinishedInCurrentSprint =  cycleTimeData.status== "Done" && bCompletedDateInThisSprint;
		
		// Test check - this really needs to be checking start and end date of the sprints....
		var bFinishedInPreviousSprint = cycleTimeData.status== "Done" && ( cycleTimeData.closedDate < sprintStartedDate)
		if (bFinishedInCurrentSprint && bFinishedInPreviousSprint) {
			console.log("think end date incorrect for "+issueInSprint.key+ " "+sprintName+ " < "+lastSprintName );
		}

		if ( ((bCompletedStoryOnly && bFinishedInCurrentSprint) || !bCompletedStoryOnly) && !bFinishedInPreviousSprint) {

			var fields = issueInSprint.fields;
			var changelog = issueInSprint.changelog;

			var storyToSave = {id: 		issueInSprint.id,
							   key: 	issueInSprint.key,
							   status: 	fields.status.name,
							   summary:  fields.summary
							   };

			storyToSave.cycleTime 			= Math.min(cycleTimeData.cycleTime, maxDaysToShow);
			storyToSave.analysisStartedDate 	= cycleTimeData.analysisStartedDate;
			storyToSave.devStartedDate 	= cycleTimeData.devStartedDate;
			storyToSave.closedDate 			= cycleTimeData.closedDate;

			// if finished in this or a previous sprint
			storyToSave.realStatus = (bFinishedInCurrentSprint || bFinishedInPreviousSprint) ? "Done": "Unfinished";
			storyToSave.doneSprintName	= (storyToSave.realStatus == "Done") ? lastSprintName : "";
		  
			storyToSave.resolution = (fields.resolution != null) ? fields.resolution.name : "";
			storyToSave.fixVersion = (fields.fixVersions != null && fields.fixVersions.length > 0) ? config.mapFixVersion(fields.fixVersions[0].name) : "";

			// Add Linked Stories
			storyToSave.linkedIssues = getLinkedStories(fields.issuelinks);

			// Closed Sprints
			storyToSave.closedSprints = getClosedSprints(fields.closedSprints);

			storyToSave.subTasks = getSubTasks(fields.subtasks);

			storyToSave.parent = config.getParent(fields.parent, fields.customfield_10005);

			storyToSave.type = config.getPBIIssueType(fields.issuetype.name, fields.summary);

			// Story points
			//console.log("Points: "+issueInSprint.fields.customfield_10002);
			storyToSave.storyPoints = (fields.customfield_10002 >= 0) ? fields.customfield_10002 : "No Pts";


			storyToSave.project = fields.hasOwnProperty('project') ? fields.project.key : "";

			// WolfRed and Blue
			
			//console.log(storyToSave.key+" : "+storyToSave.type+" : "+storyToSave.fixVersion);

			if (bHasChangeLog) {
				storyToSave.statuses = getArrStatusChanges(changelog.histories);
				storyToSave.wasAdded = checkIfStoryAddedToSprint(storyToSave.statuses, sprintName, sprintStartedDate)
				//storyToSave.wasRemoved = checkIfStoryremovedToSprint(storyToSave.statuses, sprintClosedDate)
			}

			// If this story was completed in this sprint, then record the cycletime data
			if (bFinishedInCurrentSprint) {

				// To save graph height, max out cycletime at 200 days.			
				arrJson.push(storyToSave);
			} else {
				if (!bCompletedStoryOnly) {
					// Save anyway - to show Wolf stories that are not just development sub-tasks
					storyToSave.cycleTime = -3;
					storyToSave.extra = true;
					arrJson.push(storyToSave);
				}
			}			
		};
	});
	//console.log("Len:"+arrJson.length);
	


	// Check if any story is in the list more than once
	var arrIds = [];
	for (const item of arrJson) {
		if (arrIds.includes(item.id)) {
			console.log("ERROR!!!!!!!!!!!!!!!!!!!!! Already includes id:"+item.key+" : ");
		} else {
			arrIds.push(item.id);
		}
	}
	
	return arrJson;
}


function getStartAndEndDateForListOfSubtasksInSprint(issuesInSprint, arrSubtaskKeys) {
	var subTaskTimes = {cycleTime: -1};
	if (arrSubtaskKeys.length > 0) {
		var minDate = new Date('01-Jan-2030');
		var maxDate = new Date('01-Jan-1970');
		var minDateUsed = false;
		var maxDateUsed = false;
		for (const key of arrSubtaskKeys) {
			for (const pbi of issuesInSprint) {
				if (key == pbi.key) {
					// Get start and end data from changelog
					var cycleTimeData = analyseChangelogHistories(pbi.changelog.histories);
					if (cycleTimeData.devStartedDate != "*" && cycleTimeData.closedDate != "*") {
						var devDate 	= new Date(cycleTimeData.devStartedDate);
						var doneDate 	= new Date(cycleTimeData.closedDate);
						if (devDate < minDate) {
							minDate = devDate;
							minDateUsed = true;
						}
						if (doneDate > maxDate) {
							maxDate = doneDate;
							maxDateUsed = true;
						}
					}
				}
			}
		}
		if (minDateUsed && maxDateUsed) {
			subTaskTimes.cycleTime = dateDiffInDays(maxDate, minDate);
			subTaskTimes.minDate = minDate;
			subTaskTimes.maxDate = maxDate;
		}
	} 
	
	return subTaskTimes;
}

exports.testDays = function () {
    var results = [];
    var filename = "./data/testData.json";

    console.log("looking for "+filename);
    if (fs.existsSync(filename)) {
        console.log("Getting data from "+filename);
        let rawdata = fs.readFileSync(filename);  
        var sprint = JSON.parse(rawdata);

        var startDate = moment(sprint.startDate);
        var endDate =moment(sprint.endDate);

        startDate.hours(00);
        startDate.minutes(00);
        startDate.seconds(01);
        endDate.hours(23);
        endDate.minutes(59);
		endDate.seconds(59);
		
		var results = [];


        sprint.pbis.forEach(function (story) {
		//var story = sprint.pbis[0];
            var sprintLenInDays = 10;
			var days = [sprintLenInDays];
			var finalStatus = "";
			
			var date = moment(startDate);
			var yesterday = moment(date);
			var lastStatus = "";
            for (var ii=0; ii < sprintLenInDays; ii++) {
				var transitionsPerDay={};
				var today = date.businessAdd(ii);
				transitionsPerDay.day = today.format('ddd DD-MMM');
				
				// scan statuses to see what state it is on the defined day
				var statuses=[];
				for (const status of story.statuses) {
					var statusDate = moment(status.date);
					if (statusDate.isAfter(yesterday) && statusDate.isBefore(today)) {
						lastStatus = status.toStr;
						statuses.push({From: lastStatus});
						console.log("From: "+statusDate.format()+" "+lastStatus);
					}
				}
				transitionsPerDay.statuses = statuses;
				transitionsPerDay.lastStatus = lastStatus;
				days.push(transitionsPerDay);
				yesterday = today;
			}
			results.push(days);
			//results.push(days);
        })
    }
      
    return (results);
}

////////////////////////////////////////////////////////////////////////////////////////////////
// Private Functions
////////////////////////////////////////////////////////////////////////////////////////////////

async function getSprintDataFromAPIOrCachedFile(boardId) {
	
	var filenamebase="./data/Sprints-"+config.getDataFilePrefix()+"-"+boardId;
	var arrResults=[];
	
	var startAt = config.getSprintIndexStartAt();;

	// maxResults = could be larger than 50!
	var maxResults = 50;
	var sprints;

	do {
		var filename = startAt==0 ? filenamebase +".json" : filenamebase + "-"+startAt+".json";

		//console.log("looking for data from "+filename);
		if (fs.existsSync(filename)) {
			console.log("Getting data from "+filename);
			let rawdata = fs.readFileSync(filename);  
			sprints = JSON.parse(rawdata);

		} else {
			// Get Data from API and save it for future use
			console.log("Fetching data from api");
			
			sprints = await jira.getAllSprints(boardId, startAt, maxResults);
			console.log("Type of Sprints: "+typeof(sprints) );
			if (typeof(sprints)=="string") {
				throw error("Not Logged On");
			}	
					
			outputToJsonFile(sprints, filename);
		} 
		startAt += maxResults;

		arrResults.push(...sprints.values);
		
	} while (!sprints.isLast)
	return arrResults;
}



function analyseChangelogHistories(histories) {
	//var previousDate= new Date();
	var bDebug = false;

	var analysisInProgDate="*";
	var arrDevInProgDates=[];
	var devReadyForTestingDate="*";
	var devTestingDate="*";
	var cycleTimeData = {};
	setCycleTimeToNotDone(cycleTimeData);
	
	const strTypeStatus = "status";
	const strDEVINPROGRESS = config.getStrDevInProgress();
	const strANALYSIS = "In Analysis";
	const strDEVREADYFORTESTING = "Ready for Test";
	const strDEVTESTING = "In Test"
	

	for (let history of histories) {
		debug(bDebug, "history: "+ history.id+" "+history.items.length);
		for (let item of history.items) {
			if (item.field == strTypeStatus) {
				var createdDate = new Date(history.created);
				
				if (item.toString == strANALYSIS && analysisInProgDate=="*") {
					debug(bDebug, "set data: "+ item.toString);
					analysisInProgDate = createdDate;
				}

				// if this is the first time though, set the set
				// If this multiple times though, reset the end date
				if (item.toString == strDEVINPROGRESS) {
					arrDevInProgDates.push(createdDate);

					debug(bDebug,"set data: "+ item.toString);
					
					setCycleTimeToNotDone(cycleTimeData);
				}
				if (item.toString == strDEVREADYFORTESTING && devReadyForTestingDate=="*") {
					debug(bDebug,"set data: "+ item.toString);
					devReadyForTestingDate = createdDate;
				}				
				if (item.toString == strDEVTESTING && devTestingDate=="*") {
					debug(bDebug,"set data: "+ item.toString);
					devTestingDate = createdDate;
				}
				//if (item.toString == strDEVTESTING) {
				//	devTestingDate = createdDate;
				//}
				// Stop at whichever end case (Closed or Done for Sid) comes first
				if (item.toString == config.getStrClosed(createdDate)) {
					if (arrDevInProgDates.length > 0) {
						debug(bDebug, history.id+" "+history.created.toString());
						cycleTimeData.cycleTime = dateDiffInDays(createdDate, arrDevInProgDates[0]);
						cycleTimeData.closedDate = createdDate;
						cycleTimeData.status = "Done";
						cycleTimeData.devStartedDate = arrDevInProgDates[0];
						cycleTimeData.analysisStartedDate = analysisInProgDate;
						cycleTimeData.testStartedDate = devTestingDate;
					} else {
						// If from In Analysis straight to done
						cycleTimeData.cycleTime = 0;
						cycleTimeData.status = "Done";
					}

				}
			} else if (item.field == "Sprint") {
				var sprintList = item.toString;
				if (sprintList.indexOf(", ") == -1) {
					cycleTimeData.lastSprint = sprintList;
				} else {
					var sprints = sprintList.split(", ");
					cycleTimeData.lastSprint = sprints[sprints.length-1];
				}

			}
		}

	}
	
	return cycleTimeData;
}

function debug(bShow, string) {
	if (bShow) {
		console.log(string);
	}
}

function setCycleTimeToNotDone(objCycleTime) {
	objCycleTime.cycleTime =-1
	objCycleTime.devStartedDate = "";
	objCycleTime.closedDate = "";
	objCycleTime.status = "Not Done";
}




/* Files stored in order to migitate network issues
 * Files match jira query responses
 */
async function getJiraIssueDataFromAPIOrCachedFile(sprintId, bSaveSprintToFile=true) {
	console.log("getJiraIssueDataFromAPIOrCachedFile "+sprintId);
	var filenamebase="./data/"+config.getDataDir()+"/"+config.getDataFilePrefix()+"Sprints-"+sprintId;
	var filename = filenamebase +".json";
	var arrResults=[];
	
	var startAt = 0;
		
	// maxResults = could be larger than 50!
	var maxResults = 100;

	var jiraQueryResult;
	var isLast = false;

	do {

		var filename = startAt==0 ? filenamebase +".json" : filenamebase + "-"+startAt+".json";;

		console.log("looking for "+filename);
		if (fs.existsSync(filename)) {
			try {
				console.log("Getting data from "+filename);
				let rawdata = fs.readFileSync(filename);  
				jiraQueryResult = JSON.parse(rawdata);		
			} 
			catch (err) {
				console.log(err);
			}

		} else {
			// Get Data from API and save it for future use
			console.log("Fetching data from api: "+config.getCurrentTeam()+"-"+sprintId+"-"+startAt);

			var expand='changelog';		

			jiraQueryResult = await getIssuesForSprint(sprintId, startAt, maxResults, expand);
			
			if (typeof jiraQueryResult == "string") {
				throw "getIssuesForSprint returning string ("+sprintId + " : "+startAt+") "+filename;
			}				
			
			//output to file if this is closed sprint.
			if (bSaveSprintToFile) {
				outputToJsonFile(jiraQueryResult, filename);
			}
		}

		arrResults.push(...jiraQueryResult.issues);	

		isLast = jiraQueryResult.startAt+jiraQueryResult.maxResults >= jiraQueryResult.total;
		startAt += maxResults;
		
	} while (isLast == false);
	
	
	return arrResults;
}

function getLinkedStories(arrJiraIssueLinks) {
	var arrJsonLinks = [];

	if (typeof arrJiraIssueLinks !== 'undefined' && arrJiraIssueLinks.length> 0) {
	
		for (const linkedIssue of arrJiraIssueLinks) {
			var description;
			var linkedIssueDetails;

			if (linkedIssue.hasOwnProperty('inwardIssue')){
				description = linkedIssue.type.inward;
				linkedIssueDetails = linkedIssue.inwardIssue;
			} else {
				description = linkedIssue.type.outward;
				linkedIssueDetails = linkedIssue.outwardIssue;
			}
			var newFlattenedLinkedIssue = {	description: description, 
											key: linkedIssueDetails.key,
											status: linkedIssueDetails.fields.status.name,
											type: linkedIssueDetails.fields.issuetype.name};
			arrJsonLinks.push(newFlattenedLinkedIssue);
		}
	}
	return arrJsonLinks;
}

function getClosedSprints(arrJiraClosedSprints) {
	var arrClosedSprints = [];
	if (typeof arrJiraClosedSprints !== 'undefined' && arrJiraClosedSprints.length> 0) {
		for (const closedSprint of arrJiraClosedSprints) {
			arrClosedSprints.push(closedSprint.name);
		}
	}
	return arrClosedSprints;
}

function getSubTasks(arrJiraSubtasks) {
	var arrSubtasks = [];
	if (typeof arrJiraSubtasks !== 'undefined' && arrJiraSubtasks.length> 0) {
		for (const subtask of arrJiraSubtasks) {
			arrSubtasks.push(subtask.key);
		}
	}
	return arrSubtasks;
}



function getArrStatusChanges(histories) {
	var statuses = [];

	var lastDate="*"

	var StrSTATUS = "status"

	for (const history of histories) {
		for (const item of history.items) {
			switch (item.field) {
				case "Link":
				case "Acceptance Criteria":
				case "description":
				case "Rank":
				case "assignee":
				case "Attachment":
				case "ProjectImport":
				case "Workflow":
				case "summary":
				case "Fix Version":
				case "Comment":
				case "Component":
				case "labels":
				case "priority":
				case "RemoteIssueLink":
				case "timeestimate":
				case "timeoriginalestimate":
				case "Steps to reproduce":
				case "Key":
				case "project":
				case "WorklogId":
				case "timespent":
					break;
				default:
					// calculate diff days from last status for status changes
					var diffDays = 0;
					if ( item.field == StrSTATUS) {
						var dateCreated;
						if (lastDate != "*") {
							dateCreated = new Date(history.created);
							diffDays = dateDiffInDays(dateCreated, lastDate);
						}						
						lastDate = new Date(history.created);
					}
					

					var shortItem = {type: item.field, 
									fromStr: item.fromString,
									toStr: item.toString, 
									date: history.created, 
									diff: diffDays };
					statuses.push(shortItem);
			}
		}
	}
	return statuses;
}

function checkIfStoryAddedToSprint(statuses, sprintName, sprintStartedDate) {
	bWasAdded = false;
	var dateSprintStarted = new Date(sprintStartedDate);
	for (const status of statuses) {
		if ((status.toStr == sprintName) && (new Date(status.date) > dateSprintStarted)) {
			bWasAdded = true;
			break;
		}
	}
	return bWasAdded;
}


async function getIssuesForSprint(sprintId) {
	console.log("getIssuesForSprint: "+sprintId);
    // var startAt = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 0;
	// var maxResults = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 50;

	var startAt = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
	var maxResults = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 50;
	var expand = arguments[3];

	return jira.doRequest(jira.makeRequestHeader(jira.makeAgileUri({
		pathname: '/sprint/' + sprintId + '/issue',
		query: {
		  startAt: startAt,
		  maxResults: maxResults,
		  expand: expand || ''
		}
	})));
}



/////////////////////////////////////////////////////////////////////////
// Utility Functions
/////////////////////////////////////////////////////////////////////////
function dateDiffInDays(date1, date2) {
	try {
		
		var diff = moment(date1).businessDiff(moment(date2));
		return (isNaN(diff) ? 0 : diff);
	} 
	catch (error) {
		console.log(error);
	}

}

function createEmptyFile(fileName) {
	fs.closeSync(fs.openSync(fileName, 'w'));
}


function outputToJsonFile(arrJson, fileName) {
	console.log("Writing file: Len- "+arrJson.length);
	var fullFileName = fileName;
	//for (const jsonItem of arrJson) {
		try {
			createEmptyFile(fullFileName);
			//writeFileLine(JSON.stringify(arrJson), fileName, false);
			fs.writeFileSync(fullFileName, JSON.stringify(arrJson), function(err) {
				if (err) throwerr;
			
			})
		} catch (error) {
			console.log("Failed write");
		}
	//}
	//console.log(arrJson.length);
}


function configBusinessDays() {
	var july4th = '07-04-2015';
	var laborDay = '09-07-2015';

    var NewYearsDay18	 	= "01-Jan-2018";
    var GoodFridayBH18	 	= "30-Mar-2018";
    var EasterMondayBH18	= "02-Apr-2018";
    var EarlyMayBH18	 	= "07-May-2018";
    var SpringBH18	 		= "28-May-2018";
    var SummerBH18	 		= "27-Aug-2018";
    var ChristmasDay18	 	= "25-Dec-2018";
	var BoxingDay18	 		= "26-Dec-2018";

	var NewYearsDay19	 	= "01-Jan-2019";
    var GoodFridayBH19	 	= "19-Apr-2019";
    var EasterMondayBH19	= "22-Apr-2019";
    var EarlyMayBH19		= "06-May-2019";
    var SpringBH19	  		= "27-May-2019";
    var SummerBH19	  		= "27-Aug-2019";
    var ChristmasDay19		= "25-Dec-2019";
	var BoxingDay19	  		= "26-Dec-2019";
	moment.updateLocale('en', {
	holidays: [NewYearsDay18, GoodFridayBH18, EasterMondayBH18, EarlyMayBH18, SpringBH18, SummerBH18, ChristmasDay18, BoxingDay18,
			   NewYearsDay19, GoodFridayBH19, EasterMondayBH19, EarlyMayBH19, SpringBH19, SummerBH19, ChristmasDay19, BoxingDay19],
	holidayFormat: 'DD-MMM-YYYY'
	});
}

function standardDeviation(values){
	var avg = average(values);
	
	var squareDiffs = values.map(function(value){
	  var diff = value - avg;
	  var sqrDiff = diff * diff;
	  return sqrDiff;
	});
	
	var avgSquareDiff = average(squareDiffs);
  
	var stdDev = Math.sqrt(avgSquareDiff);
	return stdDev;
  }
  
  function average(data){
	var sum = data.reduce(function(sum, value){
	  return sum + value;
	}, 0);
  
	var avg = sum / data.length;
	return avg;
  }